import 'package:flutter/material.dart';
import 'package:rescueapp/start_triage_system/model/startTriage.dart';
import 'package:rescueapp/start_triage_system/widgets/ambulatory.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'KatApp',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage());
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    StartTriage startTriage = new StartTriage();
    return Scaffold(
      appBar: AppBar(
        title: Text('KatApp Homepage'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 250,
            ),
            SizedBox(
                width: 200,
                height: 50,
                child: Container(
                    child: RaisedButton(
                  child: Text('ÜBUNG',
                      style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                      )),
                  color: Colors.green,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Ambulatory(startTriage)),
                    );
                  },
                ))),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              height: 50,
              child: Container(
                  child: RaisedButton(
                      child: Text('REAL',
                          style: TextStyle(fontSize: 22, color: Colors.white)),
                      color: Colors.red,
                      onPressed: null)),
            ),
          ],
        ),
      ),
    );
  }
}
