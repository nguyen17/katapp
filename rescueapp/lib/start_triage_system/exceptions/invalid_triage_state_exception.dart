class InvalidTriageStateException implements Exception {
  final String message;
  InvalidTriageStateException(this.message);
  String toString() => 'InvalidTriageStateException: $message';
}
