enum StartTriageState {
  Ambulatory,
  SpontaneousBreathingPresent,
  AfterOpeningAirways,
  BreathingFrequencyPerMinute,
  CapillaryFillingTime,
  Neurology,
  CameraAccess,
  LockScreen,
  ObjectState
}
