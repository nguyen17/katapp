import 'package:flutter/material.dart';
import '../model/startTriage.dart';
import '../enums/start_triage_state.dart';
import 'lock_screen.dart';

class CameraAccess extends StatefulWidget {
  final String title;
  final StartTriage startTriage;

  CameraAccess(
    StartTriage startTriage, {
    Key key,
    this.title,
  })  : startTriage = startTriage,
        super(key: key) {
    startTriage.setState(StartTriageState.CameraAccess);
  }

  @override
  _CameraAccessState createState() => _CameraAccessState();
}

class _CameraAccessState extends State<CameraAccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("KatApp"),
        backgroundColor: Colors.blue,
        actions: <Widget>[],
      ),
      body: Builder(
        builder: (BuildContext context) {
          return Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                    height: 40,
                    child: Container(
                      alignment: Alignment.center,
                      child: Text('Bitte machen Sie ein Foto des Patienten',
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 22,
                              color: Colors.blue[900])),
                    )),
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                  height: 250,
                  width: 250,
                  child: RaisedButton(
                      child: Icon(Icons.photo_library,
                          color: Colors.black, size: 35),
                      color: Colors.grey,
                      onPressed: null),
                ),
                SizedBox(
                  height: 50,
                ),
                SizedBox(
                  height: 50,
                  width: 200,
                  child: RaisedButton(
                      child: Text('Absenden',
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                      color: Colors.red,
                      onPressed: () {
                        widget.startTriage.getTriageCategory();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  LockScreen(widget.startTriage)),
                        );
                      }),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                    width: 200,
                    height: 50,
                    child: Container(
                        child: RaisedButton(
                            child: new Text('Zurück',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white)),
                            color: Colors.blue[900],
                            onPressed: () {
                              Navigator.pop(context);
                            }))),
              ],
            ),
          );
        },
      ),
    );
  }
}
