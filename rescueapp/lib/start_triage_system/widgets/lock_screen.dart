import 'package:flutter/material.dart';
import '../model/startTriage.dart';
import '../enums/start_triage_state.dart';

class LockScreen extends StatefulWidget {
  final String title;
  final StartTriage startTriage;

  LockScreen(StartTriage startTriage, {Key key, this.title})
      : startTriage = startTriage,
        super(key: key) {
    startTriage.setState(StartTriageState.LockScreen);
  }

  @override
  _LockScreenState createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.startTriage.getTriageCategory().getColor(),
      appBar: AppBar(
        title: Text('KatApp'),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
        child: Column(
          children: <Widget>[
            SizedBox(
                height: 200,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                      widget.startTriage.getTriageCategory().getCategory(),
                      style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.w900,
                          color: Colors.white)),
                )),
            SizedBox(
                width: 250,
                height: 40,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text('PatientenID:',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue[900],
                      )),
                )),
            SizedBox(
                width: 250,
                height: 40,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text('1',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue[900],
                      )),
                )),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                width: 250,
                height: 40,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text('Standort:',
                      style: TextStyle(fontSize: 20, color: Colors.blue[900])),
                )),
            SizedBox(
                width: 250,
                height: 90,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                      'Hochschule Reutlingen \nAlteburgstraße 150 \n72762 Reutlingen',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue[900],
                      )),
                )),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                width: 250,
                height: 40,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text('Datum/ Uhrzeit:',
                      style: TextStyle(fontSize: 20, color: Colors.blue[900])),
                )),
            SizedBox(
                width: 250,
                height: 50,
                child: Container(
                  alignment: Alignment.centerLeft,
                  child: Text(widget.startTriage.getDateTime(),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.blue[900],
                      )),
                )),
            SizedBox(
              height: 50,
            ),
            SizedBox(
                width: 300,
                height: 50,
                child: Container(
                    child: RaisedButton(
                        child: new Text('Eingabe rückgängig machen',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: Colors.white)),
                        color: Colors.blue[900],
                        onPressed: () {
                          Navigator.pop(context);
                        }))),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                width: 300,
                height: 50,
                child: Container(
                    child: RaisedButton(
                        child: new Text('Zweite Sichtung',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: Colors.white)),
                        color: Colors.grey,
                        onPressed: null))),
          ],
        ),
      ),
    );
  }
}
