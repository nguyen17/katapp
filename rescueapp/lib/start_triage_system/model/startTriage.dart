import 'package:flutter/material.dart';
import '../enums/start_triage_state.dart';
import '../exceptions/invalid_triage_state_exception.dart';

class StartTriage {
  bool _isAmbulatory;
  bool _isSpontaneousBreathingPresent;
  bool _isAfterOpeningAirways;
  bool _isBreathingFrequencyPerMinute;
  bool _isCirculation;
  bool _isNeurology;

  Object objectState = StartTriageState.ObjectState;
  TriageCategory _triageCategory = new TriageCategory();
  StartTriageState _startTriageState = StartTriageState.ObjectState;

  String getDateTime() {
    DateTime now = DateTime.now();
    String currentTime =
        "${now.day.toString()}-${now.month.toString().padLeft(2, '0')}"
        "-${now.year.toString().padLeft(2, '0')}/"
        " ${now.hour.toString()}:${now.minute.toString()}:${now.second.toString()}";
    return currentTime;
  }

  bool getIsAmbulatory() {
    return _isAmbulatory;
  }

  bool getIsSpontaneousBreathingPresent() {
    return _isSpontaneousBreathingPresent;
  }

  bool getIsAfterOpeningAirways() {
    return _isAfterOpeningAirways;
  }

  bool getIsBreathingFrequencyPerMinute() {
    return _isBreathingFrequencyPerMinute;
  }

  bool getIsCirculation() {
    return _isCirculation;
  }

  bool getIsNeurology() {
    return _isNeurology;
  }

  void setIsSpontaneousBreathingPresent(bool isSpontaneousBreathingPresent) {
    switch (_startTriageState) {
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.SpontaneousBreathingPresent:
      case StartTriageState.Ambulatory:
      case StartTriageState.AfterOpeningAirways:
      case StartTriageState.BreathingFrequencyPerMinute:
      case StartTriageState.Neurology:
      case StartTriageState.CapillaryFillingTime:
        this._isSpontaneousBreathingPresent = isSpontaneousBreathingPresent;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setIsAfterOpeningAirways(bool isAfterOpeningAirways) {
    switch (_startTriageState) {
      case StartTriageState.AfterOpeningAirways:
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.Ambulatory:
      case StartTriageState.SpontaneousBreathingPresent:
        this._isAfterOpeningAirways = isAfterOpeningAirways;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setBreathingFrequencyPerMinute(bool isBreathingFrequencyPerMinute) {
    switch (_startTriageState) {
      case StartTriageState.BreathingFrequencyPerMinute:
      case StartTriageState.SpontaneousBreathingPresent:
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.Ambulatory:
      case StartTriageState.Neurology:
      case StartTriageState.CapillaryFillingTime:
        this._isBreathingFrequencyPerMinute = isBreathingFrequencyPerMinute;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setIsNeurology(bool isNeurology) {
    switch (_startTriageState) {
      case StartTriageState.Neurology:
      case StartTriageState.SpontaneousBreathingPresent:
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.Ambulatory:
      case StartTriageState.BreathingFrequencyPerMinute:
      case StartTriageState.CapillaryFillingTime:
        this._isNeurology = isNeurology;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setIsCirculation(bool isCirculation) {
    switch (_startTriageState) {
      case StartTriageState.SpontaneousBreathingPresent:
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.Ambulatory:
      case StartTriageState.BreathingFrequencyPerMinute:
      case StartTriageState.Neurology:
      case StartTriageState.CapillaryFillingTime:
        this._isCirculation = isCirculation;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setIsAmbulatory(bool isAmbulatory) {
    switch (_startTriageState) {
      case StartTriageState.BreathingFrequencyPerMinute:
      case StartTriageState.SpontaneousBreathingPresent:
      case StartTriageState.LockScreen:
      case StartTriageState.CameraAccess:
      case StartTriageState.Ambulatory:
      case StartTriageState.Neurology:
      case StartTriageState.CapillaryFillingTime:
      case StartTriageState.AfterOpeningAirways:
        this._isAmbulatory = isAmbulatory;
        break;
      default:
        throw new InvalidTriageStateException('InvalidTriageStateException');
        break;
    }
  }

  void setState(StartTriageState value) {
    _startTriageState = value;
    String categoryName;
    Color color;
    if (value == StartTriageState.Ambulatory ||
        value == StartTriageState.CameraAccess ||
        value == StartTriageState.CapillaryFillingTime ||
        value == StartTriageState.Neurology ||
        value == StartTriageState.AfterOpeningAirways ||
        value == StartTriageState.BreathingFrequencyPerMinute ||
        value == StartTriageState.SpontaneousBreathingPresent) {
      categoryName = "";
    } else if (value == StartTriageState.LockScreen) {
      if ((this._isAmbulatory == true)) {
        categoryName = 'Kategorie T3';
        color = Colors.green;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == false &&
          this._isAfterOpeningAirways == false) {
        categoryName = "Kategorie T4";
        color = Colors.black;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == false &&
          this._isAfterOpeningAirways == true) {
        categoryName = "Kategorie T1A";
        color = Colors.red;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == true &&
          this._isBreathingFrequencyPerMinute == true) {
        categoryName = "Kategorie T1B";
        color = Colors.red;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == true &&
          this._isBreathingFrequencyPerMinute == false &&
          this._isCirculation == true) {
        categoryName = "Kategorie T1C";
        color = Colors.red;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == true &&
          this._isBreathingFrequencyPerMinute == false &&
          this._isCirculation == false &&
          this._isNeurology == false) {
        categoryName = "Kategorie T1D";
        color = Colors.red;
      } else if (this._isAmbulatory == false &&
          this._isSpontaneousBreathingPresent == true &&
          this._isBreathingFrequencyPerMinute == false &&
          this._isCirculation == false &&
          this._isNeurology == true) {
        categoryName = "Kategorie T2";
        color = Colors.yellow;
      }
    }
    if (categoryName != null) {
      _triageCategory.setCategory(categoryName);
      _triageCategory.setColor(color);
    } else {
      throw new InvalidTriageStateException("InvalidTriageStateException");
    }
  }

  TriageCategory getTriageCategory() {
    return _triageCategory;
  }
}

class TriageCategory {
  String _category;
  Color _color;

  String getCategory() {
    return _category;
  }

  void setCategory(String category) {
    this._category = category;
  }

  Color getColor() {
    return _color;
  }

  void setColor(Color color) {
    this._color = color;
  }
}
